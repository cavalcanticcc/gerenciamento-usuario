<?php

// atd\formulario-contato\src\GerenciamentoUsuarioServiceProvider.php

namespace atd\GerenciamentoUsuario;

use Illuminate\Support\ServiceProvider;

class GerenciamentoUsuarioServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'gerenciamento-usuario');
    }

    public function register()
    {
    }
}
