<?php

// atd\formulario-contato\src\routes\web.php
Route::get('users', function(){
    return view('gerenciamento-usuario::index');
});

Route::group(['namespace' => 'atd\GerenciamentoUsuario\Http\Controllers',
    'middleware' => ['web']], function(){
    Route::get('users', 'GerenciamentoUsuarioController@index')->name('users');
//    Route::post('contato', 'FormularioContatoController@sendMail')->name('contato');
});
